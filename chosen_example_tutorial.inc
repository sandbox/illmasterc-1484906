<?php

/**
 * @file
 * This is the Chosen API Tutorial
 *
 * It goes through two examples of select elements on forms, the first using, the second not using chosen
 * while the module is enabled
 *
 */


/**
 * This first function properly uses chosen
 *
 */
function form_chosen_tutorial_1($form, &$form_state) {

  $form['selectme'] = array(
    '#type' => 'select',
    '#title' => t('A form with nothing but a textfield'),
    '#multiple' => TRUE,
    '#options' => array(
      'pretty' => 'pretty',
      'please' => 'please',
    )
  );

  return $form;
}


//////////////// Tutorial Example 2 //////////////////////

/**
 * This is Example 2, a basic form with a submit button.
 *
 * @see http://drupal.org/node/717726
 * @ingroup form_example
 */
function form_chosen_tutorial_2($form, &$form_state) {
  $form['selectme'] = array(
    '#type' => 'select',
    '#title' => t('This ought not to be the "chosen" one'),
    '#multiple' => TRUE,
    '#options' => array(
      'one' => 'one',
      'please' => 'please',
    )
  );

  // Attach library to this one
  $form['selectme2'] = array(
    '#type' => 'select',
    '#title' => t('This oughta be the chosen one'),
    '#multiple' => TRUE,
    '#options' => array(
      'pretty' => 'pretty',
      'please' => 'please',
    ),
    // This adds the js necessary
    '#pre_render' => array('chosen_pre_render'),
  );

  return $form;
}
